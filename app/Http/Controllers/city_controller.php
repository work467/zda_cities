<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\SUpport\Facades\http;
use App\models\ListItem;
/*
use \Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;*/


class city_controller extends Controller
{
    function cities(){
        $response = Http::withHeaders([
            'CURLOPT_SSL_VERTIFYPEER' => false,
            ])->get('https://raw.githubusercontent.com/TheBizii/test/main/cities.json', [
        ]);
        return view('newWiew',['cities'=>json_decode($response->body())]);

    }
    function page($id){
    $resp = json_decode(Http::withHeaders([
            'CURLOPT_SSL_VERTIFYPEER' => false,
            ])->get('https://raw.githubusercontent.com/TheBizii/test/main/cities.json', [
        ])->body());
        foreach($resp as $city)
            if($city->rank == $id){
                return view('myPage',["page"=>$city]);
            }
    }

    public function index(){
        return view('welcome',['listItems'=>ListItem::where('is_complete',0)->get()]);
    }
    public function markComplete($id){
        $listItem=ListItem::find($id);
        $listItem->is_complete=1;
        $listItem->save();

        return redirect('/');
    }
   
     public function saveItem(Request $request){
        $newItem=new ListItem;
        $newItem->city=$request->listItem;
        $newItem->Growth_from_2000_to_2013=$request->growth;
        $newItem->Latitude=$request->lat;
        $newItem->Longitude=$request->long;
        $newItem->Population=$request->pop;
        $newItem->Rank=$request->rank;
        $newItem->State=$request->state;
        $newItem->is_complete=0;
        $newItem->save();
        return redirect('/');
    }

    public function return(){
        return view('registration');
    }

   /*
   public function user($id){
        return view('newWiew',["user"=>$id]);
    }*/

    public function open(){
        return view('log');
    }

    public function build(){
        return view('builder');
    }




    /*public function insert(){
        $urlData = getURLList();
        return view('newWiew');
    }
    public function create(Request $request){
        $rules = [
			'city' => 'required|string|min:3|max:25',
			'Growth_from_2000_to_2013' => 'required|integer',
			'Latitude' => 'required|integer'
            'Longitude' => 'required|integer'
            'Population' => 'required|integer'
            'Rank' => 'required|string|integer'
            'State' => 'required|string|min:3|max:25'
		];
		$validator = Validator::make($request->all(),$rules);
		if ($validator->fails()) {
			return redirect('insert')
			->withInput()
			->withErrors($validator);
		}
		else{
            $data = $request->input();
			try{
				 $newItem=new ListItem;
                 $newItem->city=$data->['city'];
                 $newItem->Growth_from_2000_to_2013=$data->['Growth_from_2000_to_2013'];
                 $newItem->Latitude=$data->['Latitude'];
                 $newItem->Longitude=$data->['Longitude'];
                 $newItem->Population=$data->['Population'];
                 $newItem->Rank=$data->['Rank'];
                 $newItem->State=$data->['State'];
                 $newItem->is_complete=0;
				$student->save();
				return redirect('insert')->with('status',"Insert successfully");
			}
			catch(Exception $e){
				return redirect('insert')->with('failed',"operation failed");
			}
		}
    }*/

}
