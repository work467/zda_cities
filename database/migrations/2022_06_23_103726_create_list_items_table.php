<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateListItemsTable extends Migration
{
        
    public function up()
    {
      
        Schema::create('list_items', function (Blueprint $table) {
            $table->id();
            $table->string('city');
            $table->integer('Growth_from_2000_to_2013');
            $table->integer('Latitude');
            $table->integer('Longitude');
            $table->integer('Population');
            $table->integer('Rank');
            $table->string('State');
            $table->integer('is_complete');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('list_items');
    }
}
