<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
<script>
var uporabniki = ["marko", "niko", "matej", "jernej", "jan"];
	function preveri(){
		var input = document.getElementById("uporabnisko_ime");
		var vnos = input.value;
		console.log(vnos);
		if(vnos.length < 3){
			document.getElementById("napaka").innerHTML = "Uporabniško ime je prekratko";
			return false;
		}else if(vnos.length > 20){
				document.getElementById("napaka").innerHTML = "Uporabniško ime je predolgo";
				return false;
		}else if(uporabniki.includes(vnos)){
			document.getElementById("napaka").innerHTML = "Uporabniško ime že obstaja";
			return false;
		}else{
			document.getElementById("napaka").innerHTML = "";
			return true;
		}
	}
	function preveri2(){
		var input = document.getElementById("e_posta");
		var vnos = input.value;
		console.log(vnos);
		var l=/^(?=.*[@])/;
		var g=/^(?=.*[.])/;
		var m=/^(?=.*[a-z])/;
		if(l.test(vnos) == false){
			document.getElementById("napaka2").innerHTML = "Ta e-poštni naslov ne obstaja";
		}else if(g.test(vnos) == false){
			document.getElementById("napaka2").innerHTML = "Ta e-poštni naslov ne obstaja";
		}
		else if(m.test(vnos) == false){
			document.getElementById("napaka2").innerHTML = "Ta e-poštni naslov ne obstaja";
		}else{
			document.getElementById("napaka2").innerHTML = "";
		}
	}		
	function preveri3(){
		var input = document.getElementById("geslo");
		var vnos = input.value;
		console.log(vnos);
		var a=/^(?=.*[A-Z])/;
		var b=/^(?=.*[a-z])/;
		var c=/^(?=.*[0-9])/;
		if(vnos.length < 6){
			document.getElementById("napaka3").innerHTML = "Geslo je prekratko";
		}else if(a.test(vnos) == false){
			document.getElementById("napaka3").innerHTML = "Geslo mora vsebovati vsaj 1 veliko črko";
		}else if(b.test(vnos) == false){
			document.getElementById("napaka3").innerHTML = "Geslo mora vsebovati vsaj 1 malo črko";
		}else if(c.test(vnos) == false){
			document.getElementById("napaka3").innerHTML = "Geslo mora vsebovati vsaj 1 številko";
		}else{
			document.getElementById("napaka3").innerHTML = "";
		}
	}		
	function preveri4(){
		var input = document.getElementById("geslo_again");
		var vnos = input.value;
		console.log(vnos);
		var pss1 = geslo.value;
		var pss2 = geslo_again.value;
		if(pss2 == ""){
			document.getElementById("napaka4").innerHTML = "Ponovite geslo";
		}else if(pss1 != pss2){
			document.getElementById("napaka4").innerHTML = "Gesli se ne ujemata";
		}else{
			document.getElementById("napaka4").innerHTML = "";
		}
	}
	function preveri5(){
		var input = document.getElementById("male", "female");
		var vnos = input.value;
		console.log(vnos);
		if(document.getElementById("male").checked){
			document.getElementById("napaka6").innerHTML = "";
		}else if(document.getElementById("female").checked){
			document.getElementById("napaka6").innerHTML = "";
		}else{
			document.getElementById("napaka6").innerHTML = "Niste izbrali spola";
		}
	}
	
	function preveri_obrazec(){
		preveri();
		preveri2();
		preveri3();
		preveri4();
		preveri5();
		if(document.getElementById("napaka").innerHTML != ""){
			return false;
		}else if(document.getElementById("napaka2").innerHTML != ""){
			return false;
		}else if(document.getElementById("napaka3").innerHTML != ""){
			return false;
		}else if(document.getElementById("napaka4").innerHTML != ""){
			return false;
		}else if(document.getElementById("napaka6").innerHTML != ""){
			return false;
		}
		else{
		return true;
		}
	}
    function user(){
        document.write("PRIJAVLJEN");
    }
</script>
<style>
	.error {color: red;}
    table, th, td {
  border: 1px solid black;
  background-color:LightBlue;
  text-align: center;
}
table {
  width: 100%;
}
th {
  height: 50px;
}
td {
  text-align: center;
}
.header {
  background-color: Lightgray;
  padding: 30px;
  text-align: center;
}
.button {
  background-color: #4CAF50; /* Green */
  border: none;
  color: white;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
}
.button2:hover {
  box-shadow: 0 12px 16px 0 rgba(0,0,0,0.24), 0 17px 50px 0 rgba(0,0,0,0.19);
}
</style>
</head>
<body>

<h1 class="header">{{__('messages.reg')}}</h1>
<form onsubmit="return preveri_obrazec();">
<table id="prvi">
    <tr>
        <th>{{__('messages.user')}}</th>
        <th>{{__('messages.mail')}}</th>
        <th>{{__('messages.pass')}}</th>
        <th>{{__('messages.passw')}}</th>
        <th>{{__('messages.gen')}}</th>
    </tr>
    <tr>
            <td><input type="text" name="username" onchange="preveri();" id="uporabnisko_ime" style="margin-left:0px"/><label id="napaka" class="error"></label></td>
            <td><input type="text" name="email" onchange="preveri2();" id="e_posta" style="margin-left:0px"/><label id="napaka2" class="error"></label></td>
            <td> <input type="text" name="password" onchange="preveri3();" id="geslo" style="margin-left:0px"/><label id="napaka3" class="error"></label></td>
            <td><input type="text" name="rpassword" onchange="preveri4();" id="geslo_again" style="margin-left:0px"/><label id="napaka4" class="error"></label></td>
            <td><input type="radio" name="gender" onchange="preveri5();" id="male" style="margin-left:px" for="male"/>{{__('messages.male')}}<label class="error"></label>
                <input type="radio" name="gender" onchange="preveri5();" id="female" style="margin-left:8px" for="female"/>{{__('messages.female')}}<label id="napaka6" class="error"></label></td>
    </tr>
</table>
</form>
    <br>
    <br>
    <br>
    <a href="/" id="a" style=" margin-left:650px;" onclick="user()">
         <button class="button2" role="button" style="background-color: LightBlue">{{__('messages.button')}}</button>
    </a>
</body>
</html>
