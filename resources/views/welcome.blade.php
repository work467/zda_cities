<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
<style>
#a {
  margin-left: 200px;
}
.button {
  background-color: #4CAF50; /* Green */
  border: none;
  color: white;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
}
.button2:hover {
  box-shadow: 0 12px 16px 0 rgba(0,0,0,0.24), 0 17px 50px 0 rgba(0,0,0,0.19);
}
.header {
  background-color: Lightgray;
  padding: 30px;
  text-align: center;
}
.top {
  background-color: Lightblue;
}
.to {
  background-color: Lightgray;
}
</style>
</head>
<body>
<div>
    <h1 class="header">{{__('messages.welcome')}}</h1>
</div>
@foreach($listItems as $listItem)
    <div class="flex" style="align-items: center;">
        <form method="post" action="{{route('markComplete', $listItem->id)}}" accept-charset="UTF-8">
            {{csrf_field()}}
            <a href="welcome">
                <button type"submit" style="text-align: center; background-color: Lightyellow; color: blue;margin-left:600px;">{{__('messages.delete')}}: {{$listItem->city}}, id: {{$listItem->id}}</button>
            </a>
         </form>
    </div>
@endforeach
<br>
<br>
<form method="post" action="{{route('saveItem')}}" accept-charset="UTF-8">
    {{csrf_field()}}
    <label for="listItem" style="text-align: center; color: gray; margin-left:620px;" >{{__('messages.add')}}</label></br></br>
    <label for="listItem" style="text-align: center; color: black; margin-left:90px;">{{__('messages.city')}}</label>
    <label for="listItem" style="text-align: center; color: black;margin-left:80px;">{{__('messages.growth')}}</label>
    <label for="listItem" style="text-align: center; color: black;margin-left:60px;">{{__('messages.lat')}}</label>
    <label for="listItem" style="text-align: center; color: black;margin-left:120px;">{{__('messages.lon')}}</label>
    <label for="listItem" style="text-align: center; color: black;margin-left:120px;">{{__('messages.pop')}}</label>
    <label for="listItem" style="text-align: center; color: black;margin-left:140px;">{{__('messages.ran')}}</label>
    <label for="listItem" style="text-align: center; color: black;margin-left:160px;">{{__('messages.st')}}</label></br>
    <input type="text" name="listItem" style="text-align: center; color: red; margin-left:10px;" class="top">
    <input type="text" name="growth" style="text-align: center; color: red;">
    <input type="text" name="lat" style="text-align: center; color: red;" class="top">
    <input type="text" name="long" style="text-align: center; color: red;">
    <input type="text" name="pop" style="text-align: center; color: red;" class="top">
    <input type="text" name="rank" style="text-align: center; color: red;">
    <input type="text" name="state" style="text-align: center; color: red;" class="top"></br></br>
    <a href="welcome">
        <button type="submit" style="text-align: center; background-color: Lightgray; color: black; margin-left:650px;" class="button2">{{__('messages.submit')}}</button>
     </a>
</form>
</div>
<br><br>
<a href="/" id="a">
    <button class="button2" role="button">{{__('messages.button')}}</button>
 </a>
</body>
</html>
