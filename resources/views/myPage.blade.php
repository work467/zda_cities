<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
<style>
table, th, td {
  border: 1px solid red;
  background-color:LightBlue;
  text-align: center;
}
table {
  width: 100%;
}
th {
  height: 50px;
}
td {
  text-align: center;
}
h1 {
  background-color: gray;
  color: red;
}
#a {
  margin-left: 660px;
}
.dropdown {
  position: relative;
  display: inline-block;
}

.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f9f9f9;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

.dropdown:hover .dropdown-content {
  display: block;
}

.desc {
  padding: 15px;
  text-align: center;
}
.button {
  background-color: #4CAF50; /* Green */
  border: none;
  color: white;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
}
.button2:hover {
  box-shadow: 0 12px 16px 0 rgba(0,0,0,0.24), 0 17px 50px 0 rgba(0,0,0,0.19);
}
.top a:hover {
  background-color: hotpink;
}
.header {
  background-color: Lightgray;
  padding: 30px;
  text-align: center;
}
</style>
<script>
</script>
</head>
<body>

<h1 class="header">{{$page->city}}</h1>
    <table id="tabela">
    <tr>
         <th>{{__('messages.city')}}</th>
        <th>{{__('messages.growth')}}</th>
        <th>{{__('messages.lat')}}</th>
        <th>{{__('messages.lon')}}</th>
        <th>{{__('messages.pop')}}</th>
        <th>{{__('messages.ran')}}</th>
        <th>{{__('messages.st')}}</th>
    </tr>
    <tr class="top">
       <td><a a href="https://www.bing.com/search?q={{$page->city}}&qs=n&form=QBRE&msbsrank=7_8__0&sp=-1&pq={{$page->city}}&sc=15-8&sk=&cvid=179CE5A9070F4261B6020713B2184E4B">{{$page->city}}</a></td>
       <td>{{$page->growth_from_2000_to_2013}}</td>
       <td>{{$page->latitude}}</td>
       <td>{{$page->longitude}}</td>
       <td>{{$page->population}}</td>
       <td>{{$page->rank}}</td>
       <td>{{$page->state}}</td>
    </tr> 
</table>
<br>
<?php
    $api_key='78ac7daad422f11a4e545d2ae765673e';
    $api_url='https://api.openweathermap.org/data/2.5/weather?id=524901&appid=78ac7daad422f11a4e545d2ae765673e';
    $weather_data=json_decode(file_get_contents($api_url), true);
    $temperature=$weather_data['main']['temp'];
    $temperature_in_celcius=round($temperature - 273.15);
    $temperature_current_weather=$weather_data['weather'][0]['main'];
    $temperature_current_weather_description=$weather_data['weather'][0]['description'];
    $temperature_current_weather_icon=$weather_data['weather'][0]['icon'];
    echo  __('messages.w').$page->city.__('messages.s').$temperature_in_celcius.__('messages.d');
    echo"<img src='http://openweathermap.org/img/wn/".$temperature_current_weather_icon."@2x.png'/>";
?>
<br>
<br>
<div class="dropdown" style="margin-left:560px;">
   <?php
    echo"<img src='/public/image/$page->rank.jpg'/>";
    ?>
  <div class="dropdown-content">
  <div class="desc">{{$page->city}}</div>
  </div>
</div>
 <br>
 <br>
<a href="/">
    <button class="button2" role="button" color="red" style="margin-left:1200px;">{{__('messages.press')}}</button>
 </a>
</body>
</html>
