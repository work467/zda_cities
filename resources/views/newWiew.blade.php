<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
<script>
function preveri(e){
		e.preventDefault();
		var input = document.getElementById("mesto");
		var vnos = input.value;
        var izpis;
		const newDiv = $("#prvi");
		newDiv.empty();
		$.get("https://raw.githubusercontent.com/TheBizii/test/main/cities.json",function(data, status){
		data=JSON.parse(data);
       
        const head = document.createElement("tr")
        const c = document.createElement("th")
        c.innerHTML="City"
        head.append(c);
        const g = document.createElement("th")
        g.innerHTML="Growth_from_2000_to_2013"
        head.append(g);
        const la = document.createElement("th")
        la.innerHTML="Latitude"
        head.append(la);
        const lo = document.createElement("th")
        lo.innerHTML="Longitude"
        head.append(lo);
        const po = document.createElement("th")
        po.innerHTML="Population"
        head.append(po);
        const r = document.createElement("th")
        r.innerHTML="Rank"
        head.append(r);
        const s = document.createElement("th")
        s.innerHTML="State"
        head.append(s);

        newDiv.append(head);

		for(x in data){
				var a=vnos.toLowerCase()
				var b=data[x].state.toLowerCase()
					if(b.indexOf(a)>-1){
                        const newVr = document.createElement("tr")
						const newCity = document.createElement("td")
                        newCity.innerHTML=data[x].city
                        newVr.append(newCity);
                        const newGrw = document.createElement("td")
                        newGrw.innerHTML=data[x].growth_from_2000_to_2013
                        newVr.append(newGrw);
                        const newLat = document.createElement("td")
                        newLat.innerHTML=data[x].latitude
                        newVr.append(newLat);
                        const newLon = document.createElement("td")
                        newLon.innerHTML=data[x].longitude
                        newVr.append(newLon);
                        const newPop = document.createElement("td")
                        newPop.innerHTML=data[x].population
                        newVr.append(newPop);
                        const newRa = document.createElement("td")
                        newRa.innerHTML=data[x].rank
                        newVr.append(newRa);
                        const newSt = document.createElement("td")
                        newSt.innerHTML=data[x].state
                        newVr.append(newSt);
                        
                        newDiv.append(newVr);
	                }
                   else if(b.indexOf(a)==-1){
                        izpis=1;
	               };
 };
if(izpis==1){
    newDiv.append("not found");
}
});
};
$(document).ready(function(){
  $("form").on("submit", function(e){
  e.preventDefault();
  preveri(e);
  });
});
</script><meta name="viewport" content="width=device-width, initial-scale=1.0">
<style>
table, th, td {
  border: 1px solid red;
  background-color:LightBlue;
  text-align: center;
}
table {
  width: 100%;
}
th {
  height: 50px;
}
td {
  text-align: center;
}
.header {
  background-color: Lightgray;
  padding: 30px;
  text-align: center;
}
tr:nth-child(even) {
  background-color: #D6EEEE;
}
@media only screen and (max-width: 768px) {
  /* For mobile phones: */
  [class*="col-"] {
    width: 100%;
  }
}
.topnav {
  overflow: hidden;
  background-color: #333;
}
.topnav a {
  float: left;
  display: block;
  color: #f2f2f2;
  text-align: center;
  padding: 15px 16px;
  text-decoration: none;
}
.topnav a:hover {
  background-color: hotpink;
  color: black;
}
.column {
  float: left;
  width: 33.33%;
  padding: 15px;
}
.row:after {
  content: "";
  display: table;
  clear: both;
}
.top a:hover {
  background-color: orange;
}
.button {
  background-color: #4CAF50; 
  border: none;
  color: white;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
}
.button2:hover {
  box-shadow: 0 12px 16px 0 rgba(0,0,0,0.24), 0 17px 50px 0 rgba(0,0,0,0.19);
}
</style>
</head>
<body>
<h1 style="text-align: center" class="header">{{__('messages.title')}}</h1>
<div class="topnav">
  <a href="language/en">EN</a>
  <a href="language/es">SL</a>
  <a href="log" style="margin-left:1100px;">Log In</a>
  <a href="registration" style="margin-left:10px;">{{__('messages.regi')}}</a>
</div>
<br>
 <br>
 <div class="row">
 <div class="column">
    <a href="welcome" style="margin-left:100px;">
        <button class="button2" role="button" style=" background-color:LightBlue">{{__('messages.push')}}</button>
    </a>
 </div>
 <div class="column">
    <form style="margin-left:50px;">{{__('messages.filter')}}: <input type="text" name="state" id="mesto" style="margin-left:2px;"/><label id="napaka1" class="error"></label>
	    <input type="submit" value={{__('messages.r')}} name"submit" style="background-color:Lightgray" class="button2"/>
    </form>
 </div>
 <div class="column">
    <a href="builder">
	    <button class="button2" role="button" style="margin-left:250px; background-color:red" >BUILDER GAME</button>
    </a>
 </div>
 </div>
<br>
<br>
<table id="prvi">
    <tr>
        <th>{{__('messages.city')}}</th>
        <th>{{__('messages.growth')}}</th>
        <th>{{__('messages.lat')}}</th>
        <th>{{__('messages.lon')}}</th>
        <th>{{__('messages.pop')}}</th>
        <th>{{__('messages.ran')}}</th>
        <th>{{__('messages.st')}}</th>
    </tr>
    @foreach($cities as $city)
        <tr class="top">
            <td><a href="myPage/{{$city->rank}}">{{$city->city}}</a></td>
            <td><a href="myPage/{{$city->rank}}">{{$city->growth_from_2000_to_2013}}</a></td>
            <td><a href="myPage/{{$city->rank}}">{{$city->latitude}}</a></td>
            <td><a href="myPage/{{$city->rank}}">{{$city->longitude}}</a></td>
            <td><a href="myPage/{{$city->rank}}">{{$city->population}}</a></td>
            <td><a href="myPage/{{$city->rank}}">{{$city->rank}}</a></td>
            <td><a href="myPage/{{$city->rank}}">{{$city->state}}</a></td>
        </tr>
    @endforeach
</table>
</body>
</html>
