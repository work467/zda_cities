<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\city_controller;
use Illuminate\Support\Facades\App;

Route::get('/',  [city_controller::class,'cities']);

Route::get('myPage/{id}',  [city_controller::class,'page']);


Route::get('welcome',  [city_controller::class,'index']);

Route::post('/saveItemRoute',  [city_controller::class,'saveItem'])->name('saveItem');

Route::post('/markCompleteRoute/{id}',  [city_controller::class,'markComplete'])->name('markComplete');

/*Route::get('insert','city_controller@insertform');
Route::post('create','city_controller@insert');*/

Route::get('language/{locale}', function ($locale) {
    app()->setLocale($locale);
    session()->put('locale', $locale);
    return redirect()->back();
});

Route::get('registration',  [city_controller::class,'return']);
//Route::get('registration/{id}',  [city_controller::class,'user']);

Route::get('log',  [city_controller::class,'open']);

Route::get('builder',  [city_controller::class,'build']);
